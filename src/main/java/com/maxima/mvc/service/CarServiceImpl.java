package com.maxima.mvc.service;

import com.maxima.mvc.dto.Dto;
import com.maxima.mvc.model.Account;
import com.maxima.mvc.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    private final AccountRepository accountRepository;

    @Autowired
    public CarServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public void saveCar(Dto dto) {
        Account account = Account.builder()
                .brand(dto.getBrand())
                .model(dto.getModel())
                .color(dto.getColor())
                .build();

        accountRepository.save(account);
    }

    @Override
    public List<Account> getAllCars() {
        return accountRepository.getAllAccounts();
    }
}
