package com.maxima.mvc.service;

import com.maxima.mvc.dto.Dto;
import com.maxima.mvc.model.Account;

import java.util.List;

public interface CarService {

    void saveCar(Dto dto);

    List<Account> getAllCars();
}
