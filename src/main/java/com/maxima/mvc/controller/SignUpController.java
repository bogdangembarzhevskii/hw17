package com.maxima.mvc.controller;

import com.maxima.mvc.dto.Dto;
import com.maxima.mvc.service.CarService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@org.springframework.stereotype.Controller
@RequiredArgsConstructor
public class SignUpController implements Controller {

    private CarService carService;

    @Override
    public ModelAndView handleRequest(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ModelAndView modelAndView = new ModelAndView();

        if (request.getMethod().equalsIgnoreCase("post")) {
            Dto dto = Dto.builder()
                    .brand(request.getParameter("brand"))
                    .model(request.getParameter("model"))
                    .color(request.getParameter("color"))
                    .build();
            carService.saveCar(dto);

            modelAndView.setViewName("cars");
        } else {
            modelAndView.setViewName("signUp");
        }

        return modelAndView;
    }
}

